<?php

use Phalcon\Mvc\Application;

    //loader
    require_once('../app/config/loader.php');
    //database
    require_once('../app/config/di.php');
    $application = new Application($di);
try {
    echo $application->handle()->getContent( );
} catch (\Exception $e) {
    var_dump($e->getMessage());
}
