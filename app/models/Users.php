<?php
namespace MyApp\App\Models;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
class Users
{
    public $client;
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://phalconApi',
        ]);
    }

    public function register($userName , $password ,$FirstName ,$LastName , $email)
    {
        try {
            return  $this->client->post('/users/register', [
                'json' => [
                    'userName' => $userName,
                    'password' => $password,
                    'FirstName' => $FirstName,
                    'LastName' => $LastName,
                    'email' => $email
                ]
            ]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function login($userName , $password)
    {
        try {
         return $this->client->post('/users/login' , [
            'json' => [
                'userName' => $userName ,
                'password' => $password,
            ]
        ]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function resetPassword($email)
    {
        try {
            return $this->client->post('/users/reset' , [
                'json' => [
                    'email' =>  $email,
                ]
            ]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function resetForm($token , $password)
    {
        try {
            return $this->client->put('/users/resetForm' , [
                'json' => [
                    'password' =>  $password,
                    'token' => $token
                ]
            ]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function getUsers($token)
    {
        try {
            return $this->client->get('/users',['headers' => ['token' => $token]]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function getUser($token , $id)
    {
        try {
            return $this->client->get('/users/getUser',['headers' => ['token' => $token] ,
            'json' => [
                'user_id' => $id
            ]]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function updateUser($token , $id,$userName,$password,$FirstName,$LastName,$email,$role)
    {
        try {
            return $this->client->put('/users/update',['headers' => ['token' => $token] ,
                'json' => [
                    'user_id' => $id,
                    'userName' => $userName,
                    'password' => $password,
                    'firstName' => $FirstName,
                    'lastName' => $LastName,
                    'email' => $email,
                    'role' => $role,
                ]]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function deleteUser($token, $id)
    {
        $url = '/users/delete' ;
        try {
            return  $this->client->delete($url ,[
                'headers' => ['token' => $token],
                'json' => [
                    'user_id' => $id,
                ]
            ]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }

    public function getServices($token)
    {
        try {
            return  $this->client->get('/services',['headers' => ['token' => $token]]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function getAssessment($token)
    {
        try {
            return  $this->client->get('/userServices/structure/1',['headers' => ['token' => $token]]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }

    public function getAnswer($token, $answers)
    {
        $url = '/userServices/answer/1' ;
        try {
            return  $this->client->post($url ,[
                'headers' => ['token' => $token],
                'json' => [
                    'answer' => $answers,
                ]
            ]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function getResult($token)
    {
        try {
            return  $this->client->get('/userServices/result/1',['headers' => ['token' => $token]]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function getSession($token)
    {
        try {
            return  $this->client->get('/users/session',['headers' => ['token' => $token]]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function getUserService($token , $user_id)
    {
        try {
            return  $this->client->get('/userServices/session',['headers' => ['token' => $token] ,
                'json' => [
                    'user_id' => $user_id,
                    'service_id'=> 1
                ]]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
    public function CreateUserService($token , $user_id ,$username)
    {
        try {
            return  $this->client->post('/userServices',['headers' => ['token' => $token] ,
                'json' => [
                    'user_id' => $user_id,
                    'service_id'=> 1,
                    'status' => $username,
                    'start' => 1,
                    'finish' => 0,
                ]]);
        } catch (BadResponseException $e) {
            return $e->getResponse();
        }
    }
}