<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit User</title>

    <!-- Custom styles for this template -->
    {{ assets.outputCss() }}
</head>
<body class="room-editor editor state-htmlOn-cssOn-jsOn sidebar-false   layout-top  logged-out">
<form action="/dashboard/editUser/{{ id }}" method="post">
    <div class="container">
        <div class="left">
            <div class="header">
                <h2 class="animation a1">Welcome Back</h2>
                <h4 class="animation a2">Edit your user here </h4>
            </div>
            <div class="form">
                <div>
                    <label for="username">Username</label>
                    <input type="text" class="form-field animation a3 form-control" value="{{ user.userName }}"
                           name="userName" id="username">
                    {% if error is defined %}
                        <div class="invalid-feedback" style="color: red">
                            The username already taken
                        </div>
                    {% endif %}
                </div>
                <div>
                    <label for="password">password</label>
                    <input type="password" class="form-field animation a4 form-control" placeholder="password"
                           name="password"
                           id="password">
                </div>
                <div>
                    <label for="firstName">firstName</label>
                    <input type="text" class="form-field animation a4 form-control" value="{{ user.FirstName }}"
                           name="FirstName" id="firstName">
                </div>
                <div>
                    <label for="lastName">lastName</label>
                    <input type="text" class="form-field animation a4 form-control" value="{{ user.LastName }}"
                           name="LastName" id="lastName">
                </div>
                <div>
                    <label for="email">email</label>
                    <input type="email" class="form-field animation a4 form-control" value="{{ user.email }}"
                           name="email" id="email">
                </div>
                <div>
                    <label for="role">Role</label>
                    <input type="text" class="form-field animation a4 form-control" value="{{ user.role }}" name="role"
                           id="role">
                </div>
                <button type="submit" class="animation a6">EDIT</button>
            </div>
        </div>
        <div class="right"></div>
    </div>
</form>
</body>
</html>


