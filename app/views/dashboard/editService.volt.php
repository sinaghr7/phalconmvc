<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Service</title>
    <!-- Custom styles for this template -->
    <?= $this->assets->outputCss() ?>
</head>
<body class="room-editor editor state-htmlOn-cssOn-jsOn sidebar-false   layout-top  logged-out">
<form action="/dashboard/editService/<?= $id ?>" method="post">
    <div class="container">
        <div class="left">
            <div class="header">
                <h2 class="animation a1">Welcome Back</h2>
                <h4 class="animation a2">Edit your service here </h4>
            </div>
            <div class="form">
                <div>
                    <label for="title">Title</label>
                    <input type="text" class="form-field animation a3 form-control" value="<?= $service->title ?>" name="title" id="title">
                </div>
                <div>
                    <label for="description">Description</label>
                    <input type="text" class="form-field animation a4 form-control" value="<?= $service->description ?>" name="description"
                           id="description">
                </div>
                <div>
                    <label for="order">Order</label>
                    <input type="text" class="form-field animation a4 form-control" value="<?= $service->order ?>" name="order" id="order">
                </div>
                <div>
                    <label for="price">price</label>
                    <input type="text" class="form-field animation a4 form-control" value="<?= $service->price ?>" name="price" id="price">
                </div>
                <div>
                    <label for="assessment_id">Assessment</label>
                    <input type="text" class="form-field animation a4 form-control" value="<?= $service->assessment_id ?>" name="assessment_id" id="assessment_id">
                </div>
                <button type="submit" class="animation a6">EDIT</button>
            </div>
        </div>
        <div class="right"></div>
    </div>
</form>
</body>
</html>


