<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Custom Theme files -->
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <!-- //Custom Theme files -->
    <!-- web font -->
    <link href="//fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet">
    <!-- //web font -->
   {{ assets.outputCss()}}
</head>
<body>
<!-- main -->
<div class="main-w3layouts wrapper">
    <h1>Welcome</h1>
    <div class="main-agileinfo">
        <div class="agileits-top">
            <form class="was-validated" method="POST" action="/auth/register">
                <label for="validationDefault01">First name</label>
                <input class="text" type="text" name="FirstName"  required="" id="validationDefault01"
                        {% if firstName is defined %}
                    value="{{ firstName }}"
                        {% endif %}><br>
                <label for="validationDefault02">Last name</label>
                <input class="text" type="text" name="LastName"  required="" id="validationDefault02"
                        {% if lastName is defined %}
                    value="{{ lastName }}"
                        {% endif %}><br>
                <label for="validationDefault02">Email</label>
                <input class="text" type="email" name="email"  required="" id="validationDefault02"
                        {% if email is defined %}
                    value="{{ email }}"
                        {% endif %}><br>
                <div>
                <label for="validationDefault03">Username</label>
                <input class="text" type="text" name="username" required="" id="validationDefault03">
                    {% if error is not defined %}
                        <br>
                    {% endif %}
                {% if error is defined %}
                    {% if error === '["The username already taken"]' %}
                        <div class="invalid-feedback">
                            Username is Already taken
                        </div>
                        <br>
                    {% endif %}
                {% endif %}
                </div>
                <label for="validationDefault04">Password</label>
                <input class="text" type="password" name="password" required="" id="validationDefault04">
                <input type="submit" value="SIGNUP">
            </form>
            <p>Don't have an Account? <a href="/auth/login"> Login Now!</a></p>
        </div>
    </div>
    <!-- copyright -->
    <div class="colorlibcopy-agile">
        <p>© 2018 Colorlib Signup Form. All rights reserved | Design by <a href="https://colorlib.com/" target="_blank">Colorlib</a></p>
    </div>
    <!-- //copyright -->
    <ul class="colorlib-bubbles">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</div>
<!-- //main -->
</body>
</html>


