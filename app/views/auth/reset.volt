<!DOCTYPE html>
<html lang="">
<head>

    <meta charset="UTF-8">

    <title>Login</title>
    <style type="text/css">
        label {
            color: black;
        }

        .alert {
            padding: 20px;
            background-color: rgb(44, 87, 202);
            border-radius: 10px;
            color: white;
            width: 400px;
            margin: 20px auto 10px;
        }

        .closebtn {
            margin-left: 15px;
            color: white;
            font-weight: bold;
            float: right;
            font-size: 22px;
            line-height: 20px;
            cursor: pointer;
            transition: 0.3s;
        }

        .closebtn:hover {
            color: black;
        }
    </style>
    {{ assets.outputCss() }}

</head>
<body>
<div class="container">
    <div class="card">
        <div class="card-header text-center">
            Send Reset Password Link
        </div>
        <div class="card-body">
            <form action="/auth/reset" method="post">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                        else.</small>
                    {% if error is defined %}
                        <div class="alert">
                            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                            {{ error }}
                        </div>
                    {% endif %}
                </div>
                <input type="submit" name="password-reset" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
</body>
</html>
