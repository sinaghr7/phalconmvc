<main role="main" class="inner cover">
    <h1 class="cover-heading"><?= $service->title ?></h1>
    <p class="lead"><?= $service->description ?>.</p>
    <p class="lead">
        <?php if (isset($result)) { ?>
            <a href="/userservice/result" class="btn btn-lg btn-secondary">Result</a>
        <?php } ?>
        <?php if (!isset($result)) { ?>
            <a href="/services/mbti" class="btn btn-lg btn-secondary">Start the assessment</a>
        <?php } ?>
    </p>
</main>


