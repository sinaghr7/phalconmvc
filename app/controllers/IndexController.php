<?php

namespace MyApp\App\controllers;

use Phalcon\Mvc\Controller;

class IndexController extends BaseController
{
    public function indexAction()
    {
//        $this->session->destroy();
        $token = $this->session->get('token');
        $firstName = $this->session->get('FirstName');
        $lastName = $this->session->get('LastName');
        $this->view->setVar('firstName', $firstName);
        $this->view->setVar('lastName', $lastName);
        $this->view->setVar('token', $token);
        if(!$token)
        {
            $this->flashSession->error('Please login first :)');
        }else{
            return true;
        }
    }
}