<?php
namespace MyApp\App\controllers;

use Phalcon\Mvc\Controller;
use MyApp\App\Models\Users;
use MyApp\App\Models\Services;
use Phalcon\Mvc\View;
class ServicesController extends BaseController
{
    public function indexAction($id)
    {
        $this->assets->addJs('/public/js/layout.js' , true);
        $token =$this->session->get('token');
        $user_id =$this->session->get('id');
        $user = new Users();
        $service = new Services();
        $service = $service->getService($token,$id);
        $service = json_decode($service->getBody())->data;
        $this->view->service = $service;
        $test = $user->getUserService($token ,$user_id);
        $test = json_decode($test->getBody())->data;
        if ($test->finish == 1){
            $this->view->setVar('result' , 'finish');
        }
    }
    public function mbtiAction()
    {
//        $this->assets->addJs('https://cdn.jsdelivr.net/npm/vue/dist/vue.js', true);
        $this->assets->addJs('https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js' , true);
        $this->assets->addJs('/public/js/printQuestion.js', true);
        $this->view->setRenderLevel(
            View::LEVEL_ACTION_VIEW
        );
        $token =$this->session->get('token');
        $user_id =$this->session->get('id');
        $userName =$this->session->get('userName');
        $user = new Users();
        $test = $user->getUserService($token ,$user_id);
        $test = json_decode($test->getBody())->data;
        if ($test){
            $this->view->setVar('assessment_id' , $test->id);
        }else{
            $result = $user->CreateUserService($token ,$user_id , $userName);
            $userService = json_decode($result->getBody())->data;
            $this->view->setVar('assessment_id' , $userService->id);
        }
        $assessment = $user->getAssessment($token);
        $assessment = json_encode(json_decode($assessment->getBody())->data);
        $this->view->setVar('assessment', $assessment);
    }
}