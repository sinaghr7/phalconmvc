<?php


namespace MyApp\App\controllers;
use Phalcon\Mvc\Controller;

class BaseController extends Controller
{
    public function initialize()
    {
        $this->assets->addCss('/public/css/cover.css' , true);
        $this->assets->addCss('/public/css/bootstrap.min.css' , true);
        $this->view->setVar('role',$this->session->get('role'));
    }
}