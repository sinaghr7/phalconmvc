<?php

namespace MyApp\App\controllers;

use Phalcon\Mvc\Controller;
use MyApp\App\Models\Users;
use MyApp\App\Models\Services;
use Phalcon\Mvc\View;

class DashboardController extends controller
{
    public function initialize()
    {
        $this->view->setVar('role', $this->session->get('role'));
    }

    public function adminAction()
    {
        $this->view->setRenderLevel(
            View::LEVEL_ACTION_VIEW
        );
        $token = $this->session->get('token');
        $user = new Users();
        $users = $user->getUsers($token);
        $users = json_decode($users->getBody())->data;
        $services = $user->getServices($token);
        $services = json_decode($services->getBody())->data;
        $this->view->users = $users;
        $this->view->services = $services;
        return $this->view;
    }

    public function editUserAction($id)
    {
        $this->assets->addCss('/public/css/dashboard.css', true);
        $this->view->setRenderLevel(
            View::LEVEL_ACTION_VIEW
        );
        $token = $this->session->get('token');
        $user = new Users();
        $test = $user->getUser($token, $id);
        $test = json_decode($test->getBody())->data;
        $this->view->setVar('id', $id);
        $this->view->setVar('user', $test);
        $userName = $this->request->getPost('userName');
        $password = $this->request->getPost('password');
        $FirstName = $this->request->getPost('FirstName');
        $LastName = $this->request->getPost('LastName');
        $email= $this->request->getPost('email');
        $role = $this->request->getPost('role');
        $result = $user->updateUser($token, $id, $userName, $password, $FirstName, $LastName,$email, $role);
        $response = json_decode($result->getBody())->message;
        if ($response == '["The username already taken"]') {
            $this->view->setVar('error', $token);
        } elseif ($response == 'user updated successfully') {
            if ($role == 'Users' && $userName == $this->session->get('userName')) {
                $this->session->set('role', $role);
                return $this->response->redirect("/index/index");
            } else {
                return $this->response->redirect("/dashboard/admin");
            }
        }
    }

    public function deleteUserAction($id)
    {
        $this->view->setRenderLevel(
            View::LEVEL_ACTION_VIEW
        );
        $token = $this->session->get('token');
        $user = new Users();
        $test = $user->deleteUser($token, $id);
        $response = json_decode($test->getBody())->message;
        if ($response == 'user deleted successfully') {
            if ($this->session->get('id') == $id) {
                $this->session->destroy();
                return $this->response->redirect('/');
            }
            return $this->response->redirect("/dashboard/admin");
        }
    }

    public function editServiceAction($id)
    {
        $this->assets->addCss('/public/css/dashboard.css', true);
        $this->view->setRenderLevel(
            View::LEVEL_ACTION_VIEW
        );
        $token = $this->session->get('token');
        $service = new Services();
        $test = $service->getService($token, $id);
        $response = json_decode($test->getBody())->data;
        $this->view->setVar('id', $id);
        $this->view->setVar('service', $response);
        $title = $this->request->getPost('title');
        $description = $this->request->getPost('description');
        $order = $this->request->getPost('order');
        $price = $this->request->getPost('price');
        $assessment_id = $this->request->getPost('assessment_id');
        $result = $service->updateService($token, $id, $title, $description, $order, $price, $assessment_id);
        $response = json_decode($result->getBody())->message;
        if ($response == 'Service updated successfully') {
            return $this->response->redirect("/dashboard/admin");
        }
    }

    public function deleteServiceAction($id)
    {
        $this->view->setRenderLevel(
            View::LEVEL_ACTION_VIEW
        );
        $token = $this->session->get('token');
        $service = new Services();
        $test = $service->deleteService($token, $id);
        $response = json_decode($test->getBody())->message;
        if ($response == 'service deleted successfully') {
            return $this->response->redirect("/dashboard/admin");
        }
    }

    public function logoutAction()
    {
        // Destroy the whole session
        $this->session->destroy();
        return $this->response->redirect('/');
    }
}