<?php

use Phalcon\Loader;

$loader = new Loader();
$loader->registerNamespaces(
    [
        //controllers
        'MyApp\App\controllers' => '../app/controllers',
        //models
        'MyApp\App\Models' => '../app/models',
    ]
);
$loader->registerFiles(
    [
        __DIR__ . DIRECTORY_SEPARATOR. '../plugins/SecurityPlugin.php',
        __DIR__. DIRECTORY_SEPARATOR .'../../vendor/autoload.php'
    ]
);
$loader->register();